#pragma once

#include <QtWidgets/QMainWindow>

#include "ui_mainwindow.h"
#include <QStringListModel>

class VisitorModel;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = Q_NULLPTR);
private:
    Ui::MainWindowClass ui;
    QStringListModel model;
    VisitorModel* listModel;

private slots:
    void onSliderValueChanged();
    void onAddButtonClicked();
    void onDeleteSelectedButtonClicked();
};
