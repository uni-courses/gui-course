#include "mainwindow.h"

#include "Visitor.h"

#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    ui.setupUi(this);

    ui.ageSlider->setMinimum(Visitor::MinimumAge);
    ui.ageSlider->setMaximum(Visitor::MaximumAge);

    connect(ui.ageSlider, SIGNAL(valueChanged(int)), SLOT(onSliderValueChanged()));

    ui.selectedAgeLabel->setText(QString::number(Visitor::MinimumAge));

    connect(ui.addButton, SIGNAL(clicked()), SLOT(onAddButtonClicked()));
    connect(ui.deleteButton, SIGNAL(clicked()), SLOT(onDeleteSelectedButtonClicked()));
}

void MainWindow::onSliderValueChanged()
{
    ui.selectedAgeLabel->setText(QString::number(ui.ageSlider->value()));
}

void MainWindow::onAddButtonClicked()
{
    if (ui.nameLineEdit->text().isEmpty())
    {
        QMessageBox::critical(this, "Can't save", "The visitor name is not filled in.", QMessageBox::Button::Ok);
        return;
    }
    
    Visitor visitor(ui.nameLineEdit->text().toStdString(), ui.hasPetsCheckbox->isChecked(), ui.ageSlider->value());
    ui.visitorsListWidget->addItem(QString::fromStdString(visitor.toString()));
}

void MainWindow::onDeleteSelectedButtonClicked()
{
    const auto selectedIndexes = ui.visitorsListWidget->selectionModel()->selectedIndexes();
    if (selectedIndexes.isEmpty())
    {
        QMessageBox::critical(this, "Can't delete", "You haven't selected any item in the list.", QMessageBox::Button::Ok);
        return;
    }
    
    ui.visitorsListWidget->model()->removeRow(selectedIndexes.first().row());
}

