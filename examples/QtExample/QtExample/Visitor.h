#pragma once
#include <string>

class Visitor
{
public:
	static constexpr int MinimumAge = 18;
	static constexpr int MaximumAge = 120;
	static constexpr int AttributesCount = 3;
public:
	Visitor(const std::string& name, bool hasPets, int age);

	const  std::string& name() const noexcept;
	bool  hasPets() const noexcept;
	int age() const noexcept;

	std::string toString() const noexcept;

private:
	std::string  m_name;
	bool m_hasPets;
	int m_age;
};

